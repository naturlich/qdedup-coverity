#!/bin/sh

function deploy_docker()
{
	sh ./common/deploy.sh
}

function create_container()
{
	local ARCH=$1
	local PROJECT=$2
	local TIME=$3

	sh docker_deploy/container_setup.sh ${ARCH} "${PROJECT}-${TIME}"
	[ $? = 0 ] || exit 1
}

function destroy_container()
{
	local CONTAINER=$1

	/usr/bin/docker rm -f ${CONTAINER}
}

if [ "$#" -ne 4 ]; then
	echo "Usage: $0 project-name branch-name local-source-path git-source-path" >&2
	exit 1
fi

PROJECT=$1
BRANCH=$2
LOCAL_SRCPATH=$3
GIT_SRCPATH=$4

ARCH=x86_64

TIME="`date +%s`"
CONTAINER="${USER}-${ARCH}-${PROJECT}-${TIME}"
DOCKER_EXEC="docker exec -i ${CONTAINER} bash -c"

# Setup
deploy_docker
create_container ${ARCH} ${PROJECT} ${TIME}

# Run
${DOCKER_EXEC} "[ ! -d ${LOCAL_SRCPATH} ] || rm -rf ${LOCAL_SRCPATH}"
${DOCKER_EXEC} "git clone --recursive ${GIT_SRCPATH} ${LOCAL_SRCPATH}"
${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git pull --recurse-submodules"
${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && git submodule update --remote --recursive"

${DOCKER_EXEC} "cd ${LOCAL_SRCPATH} && scripts/coverity.sh"
${DOCKER_EXEC} "rm -rf ${LOCAL_SRCPATH}"

# clean up
destroy_container ${CONTAINER}
